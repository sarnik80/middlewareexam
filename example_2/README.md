# A mini-guide — Middleware, and how it works in Go

[Article Link](https://storytel.tech/a-mini-guide-middleware-and-how-it-works-in-go-f6076a39d8f1)

- imagine that you're sending a request to access a website in the networking world !

- this request travels through the network and reaches the desired server (az tarigh network be server mord nazar mirese) .

- farz konid in server baraye pardazesh kardan request shoma bayad yeksery amaliat ghabli ro anjam bede masalan ghabl az inke beheton list karhaton ro neshon bede bayad login shode bashid , khob server bayad check kone shoma login kardin ya na

- now instead of the server implementing all the necessary operations in a
  single function it can uses middlewares .

- **middleware** acts as intermediate layers that receive your request and perform specific operations on it .

- they then pass the request to the next middleware and this process continues until it reaches the final middleware where response is generated for your request .

- Similar to a chain , middleware functions are executed
  in a specific order va harkodom az onha kar khas khodeshon ro ok mikonan
  ta darnahayat request asli berese be server .

- by using middleware , various functionalities can be added ,
  such as user authentication , statistics loggin , caching ...

- middleware enables reusability and helps in dividing the code into smaller , manageable components .

- ![chaining handlers][pic1]

[pic1]: https://gitlab.com/sarnik80/middlewareexam/uploads/1a351405d45ab6853049063678b658dc/Screenshot_from_2023-08-07_22-04-33.png

---

- ba tavajoh be image we can pass a function f1 into another function f2 for f2 to do its processing , and then call f1 ;
