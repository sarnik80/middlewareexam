package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

func main() {

	router := mux.NewRouter()

	router.Methods("GET").Path("/").HandlerFunc(endPointHandler)

	n := negroni.New()

	n.UseHandler(router)

	http.ListenAndServe(":8080", n)
}

func endPointHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint handler called")
}
